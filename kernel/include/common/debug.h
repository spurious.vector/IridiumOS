#ifndef DEBUG_H
#define DEBUG_H

// Switch to 1 for debug messages.
#define SHOW_DEBUG_MESSAGES 0

#if SHOW_DEBUG_MESSAGES
#define DEBUG
#endif

#endif
